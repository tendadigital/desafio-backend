# Desafio Técnico

Este Desafio Técnico faz parte do processo seletivo para a vaga de Desenvolvedor.

Se topar o desafio, crie um repositório contendo a sua solução, escreva um arquivo `README.md` explicando o seu projeto e como podemos executá-lo.

---

### O problema:
A equipe de Marketing da sua empresa precisa de uma maneira de criar links curtos e fáceis de lembrar para incluir em publicações nas redes sociais, principalmente em imagens, onde não é possível selecionar e copiar uma URL.

### Solução:
Você deve criar uma API que possa receber uma URL qualquer e um nome/path (opcional, ficará na URL final se for informado) e forneça um link curto que redirecione para a URL original. 

Você também pode complementar a sua solução com outras funcionalidades, por exemplo:

* Guardar uma thumbnail da URL encurtada (não precisa se preocupar com o frontend!)
* Registrar as visitas que um link já teve
* Categorizar links
* Invalidar links
* Expirar links com base no tempo de vida
* Expirar links com base no número de visitas
* Uma interface para gerenciar os links

Obs.: Não precisa se limitar à esta lista de funcionalidades, pode complementar sua solução como quiser.

Você **deve** usar NodeJS com quaisquer bibliotecas, bancos de dados e serviços que quiser para completar a sua tarefa.

**Dica**: Você pode usar diversos recursos gratuitos para desenvolvedores listados em https://free-for.dev/, se quiser!

### O que será avaliado:

* Arquitetura da solução
* Qualidade do código
* Testes e validações de dados
* Criatividade

---

**Boa sorte!**
